﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Adform.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Adform.Controllers
{
    [Produces("application/json")]
    [Route("api/ItemAPI")]
    public class ItemAPI : Controller
    {
        private readonly ItemContext _context;

        public ItemAPI(ItemContext context)
        {
            _context = context;
        }

        // GET: api/<controller>
        [HttpGet]
        [Route("List")]
        public IActionResult GetItems()
        {
            return new RedirectToPageResult("/Shopping/ItemList");
        }

        // GET api/values/5 
        [HttpGet]
        [Route("Details/{ItemName}")]
        public IEnumerable<Item> GetItemDetails(string ItemName)
        {
            return _context.Item.Where(i => i.ItemName == ItemName).ToList();
        }

        //// POST api/<controller>
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
