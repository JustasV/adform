﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Adform.Data
{
    public class Item
    {
        [Key]
        public int ItemID { get; set; }

        [Required]
        [Display(Name = "ItemName")]
        public string ItemName { get; set; }

        [Required]
        [Display(Name = "ItemPrice")]
        public decimal ItemPrice { get; set; }

        [Display(Name = "ImageName")]
        public string ImageName { get; set; }
        
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "UsernameCreated")]
        public string UsernameCreated { get; set; }

        [Required]
        [Display(Name = "DateCreated")]
        public DateTime DateCreated { get; set; }
    }
}
