using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Adform.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Adform.Views.Shopping
{
    public class ItemListModel : PageModel
    {
        public IEnumerable<Item> Items { get; set; }

        private readonly ItemContext _itemContext;

        public ItemListModel(ItemContext itemContext)
        {
            this._itemContext = itemContext;
        }

        public void OnGet()
        {
            Items = _itemContext
                        .Item
                        .OrderBy(i => i.ItemName)
                        .Take(10);
        }
    }
}