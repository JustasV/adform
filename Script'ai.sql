
  DECLARE @MaxCount INT = 10
  DECLARE @CountCurrent INT = 1
  DECLARE @CountCurrentText NVARCHAR(5) = N''
  
 WHILE @CountCurrent <= @MaxCount
 BEGIN
	SET @CountCurrentText = CAST(@CountCurrent AS NVARCHAR(5))

	INSERT INTO [ShoppingDB].[dbo].[Item](
	[ItemName]
      ,[ItemPrice]
      ,[ImageName]
      ,[Description]
      ,[UsernameCreated]
      ,[DateCreated]
  ) VALUES (
	N'Test' + @CountCurrentText + ' name',
	@CountCurrent,
	N'Images/Test' + @CountCurrentText + '.png',
	N'Test ' + @CountCurrentText + ' Description',
	N'admin',
	getdate()
  )

	SET @CountCurrent = @CountCurrent + 1
 END